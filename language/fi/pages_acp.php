<?php
/**
*
* Pages extension for the phpBB Forum Software package.
* Finnish translation by Sami Pekkala (https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb)
*
* @copyright (c) 2014 phpBB Limited <https://www.phpbb.com>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	// Manage page
	'ACP_PAGES_MANAGE'					=> 'Sivujen hallinta',
	'ACP_PAGES_MANAGE_EXPLAIN'			=> 'Tässä voit luoda, muokata ja poistaa vapaamuotoisia muuttumattomia sivuja.',
	'ACP_PAGES_CREATE_PAGE'				=> 'Luo sivu',
	'ACP_PAGES_CREATE_PAGE_EXPLAIN'		=> 'Alla olevalla lomakkeella voit luoda uuden vapaamuotoisen sivun keskustelupalstallesi.',
	'ACP_PAGES_EDIT_PAGE'				=> 'Muokkaa sivua',
	'ACP_PAGES_EDIT_PAGE_EXPLAIN'		=> 'Alla olevalla lomakkeella voit päivittää keskustelupalstallesi lisättyä vapaamuotoista sivua.',

	// Display pages list
	'ACP_PAGES_TITLE'					=> 'Otsikko',
	'ACP_PAGES_DESCRIPTION'				=> 'Kuvaus',
	'ACP_PAGES_ROUTE'					=> 'Reitti',
	'ACP_PAGES_TEMPLATE'				=> 'Sivupohja',
	'ACP_PAGES_ORDER'					=> 'Järjestys',
	'ACP_PAGES_LINK'					=> 'Linkki',
	'ACP_PAGES_VIEW'					=> 'Näytä sivu',
	'ACP_PAGES_STATUS'					=> 'Tila',
	'ACP_PAGES_PUBLISHED'				=> 'Julkaistu',
	'ACP_PAGES_PUBLISHED_NO_GUEST'		=> 'Julkaistu (vain jäsenille)',
	'ACP_PAGES_PRIVATE'					=> 'Yksityinen',
	'ACP_PAGES_EMPTY'					=> 'Sivuja ei ole',

	// Purge icons
	'ACP_PAGES_PURGE_ICONS'				=> 'Tyhjennä välimuisti',
	'ACP_PAGES_PURGE_ICONS_LABEL'		=> 'Tyhjennä sivujen kuvakkeiden välimuisti',
	'ACP_PAGES_PURGE_ICONS_EXPLAIN'		=> 'Uudet mukautetut sivulinkkien kuvakkeet eivät välttämättä näy ilman kuvakevälimuistin tyhjentämistä. Mukautetut kuvakkeet tulee tallentaa phpBB:n <samp>styles/*/theme/images/</samp>-hakemistoihin. Kuvaketiedostojen nimien täytyy olla muodossa <samp>pages_reitti.gif</samp>, missä <samp>reitti</samp> vastaa sivun reittiä.',

	// Messages shown to user
	'ACP_PAGES_DELETE_CONFIRM'			=> 'Haluatko varmasti poistaa tämän sivun?',
	'ACP_PAGES_DELETE_SUCCESS'			=> 'Sivu on poistettu.',
	'ACP_PAGES_DELETE_ERRORED'			=> 'Sivun poisto epäonnistui.',
	'ACP_PAGES_ADD_SUCCESS'				=> 'Sivu on luotu.',
	'ACP_PAGES_EDIT_SUCCESS'			=> 'Sivu on päivitetty.',

	// Add/edit page
	'ACP_PAGES_SETTINGS'				=> 'Sivun tiedot',
	'ACP_PAGES_OPTIONS'					=> 'Sivun asetukset',
	'ACP_PAGES_FORM_TITLE'				=> 'Sivun otsikko',
	'ACP_PAGES_FORM_TITLE_EXPLAIN'		=> 'Tämä kenttä on pakollinen.',
	'ACP_PAGES_FORM_DESC'				=> 'Sivun kuvaus',
	'ACP_PAGES_FORM_DESC_EXPLAIN'		=> 'Tämä näkyy vain ylläpidon hallintapaneelin sivuluettelossa.',
	'ACP_PAGES_FORM_ROUTE'				=> 'Sivun URL-reitti',
	'ACP_PAGES_FORM_ROUTE_EXPLAIN'		=> 'Sivun nimen puhdistettu versio, jonka perusteella sivun URL muodostetaan (esim. <samp>http://www.phpbb.com/<strong>oma-reittisi</strong></samp>). Sallittuja merkkejä ovat kirjaimet, numerot, yhdysviivat ja alaviivat. Tämä kenttä on pakollinen.',
	'ACP_PAGES_FORM_CONTENT'			=> 'Sivun sisältö',
	'ACP_PAGES_FORM_CONTENT_EXPLAIN'	=> 'Sisältöä voi luoda joko normaalien phpBB:n BBCode-tagien, hymiöiden ja automaattisten linkkien avulla tai vaihtoehtoisesti HTML-tilassa. HTML-tilassa BBCode-tagit, hymiöt ja automaattiset linkit eivät toimi, mutta voit vapaasti käyttää mitä tahansa kelvollista HTML-koodia. Huomaa, että sisältö lisätään jo olemassa olevaan HTML-sivupohjaan, joten älä käytä DOCTYPE-, HTML-, BODY- tai HEAD-tageja. Kaikki muut HTML-tagit, kuten IFRAME-, SCRIPT-, STYLE-, EMBED- ja VIDEO-tagit, ovat kuitenkin sallittuja.',
	'ACP_PAGES_FORM_TEMPLATE'			=> 'Sivupohja',
	'ACP_PAGES_FORM_TEMPLATE_EXPLAIN'	=> 'Mukautettuja sivupohjia, joiden nimet ovat muodossa <samp>pages_*.html</samp>, voi lisätä phpBB:n <samp>styles/*/template</samp>-hakemistoihin.',
	'ACP_PAGES_FORM_TEMPLATE_SELECT'	=> 'Valitse sivupohja',
	'ACP_PAGES_FORM_ORDER'				=> 'Sivun järjestys',
	'ACP_PAGES_FORM_ORDER_EXPLAIN'		=> 'Sivuille osoittavien linkkien järjestys määräytyy tämän kentän perusteella. Linkit asetetaan suuruusjärjestykseen pienimmästä arvosta alkaen.',
	'ACP_PAGES_FORM_LINKS'				=> 'Sivulinkin sijainnit',
	'ACP_PAGES_FORM_LINKS_EXPLAIN'		=> 'Valitse sijainnit, joissa tälle sivulle osoittava linkki voi esiintyä. Pidä pohjassa CTRL-näppäintä (tai CMD-näppäintä Mac-tietokoneilla) valitaksesi useita eri vaihtoehtoja.',
	'ACP_PAGES_FORM_ICON_FONT'			=> 'Sivun linkin kuvake',
	'ACP_PAGES_FORM_ICON_FONT_EXPLAIN'	=> 'Syötä sivulle osoittavaan linkkiin yhdistettävän <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">Font Awesome</a> -kuvakkeen nimi. Jätä kenttä tyhjäksi, jos haluat käyttää Pages-laajennuksen perinteisiä CSS-/GIF-kuvakkeita.',
	'ACP_PAGES_FORM_DISPLAY'			=> 'Sivu näkyvissä',
	'ACP_PAGES_FORM_DISPLAY_EXPLAIN'	=> 'Jos valitset Ei-vaihtoehdon, sivu ei ole käytettävissä. (Huomaa: Ylläpitäjillä on silti pääsy sivulle, jotta he voivat esikatsella sivua yksityisesti kehitysvaiheen aikana.)',
	'ACP_PAGES_FORM_GUESTS'				=> 'Sivu näkyy vierailijoille',
	'ACP_PAGES_FORM_GUESTS_EXPLAIN'		=> 'Jos valitset Ei-vaihtoehdon, vain rekisteröityneet käyttäjät voivat nähdä sivun.',
	'ACP_PAGES_FORM_VIEW_PAGE'			=> 'Linkki sivulle',
	'PARSE_HTML'						=> 'HTML-tila',

	// Page link location names
	'NAV_BAR_LINKS_BEFORE'				=> 'Navigaatiopalkissa ennen linkkejä',
	'NAV_BAR_LINKS_AFTER'				=> 'Navigaatiopalkissa linkkien jälkeen',
	'NAV_BAR_CRUMBS_BEFORE'				=> 'Navigaatiopalkissa ennen polkua',
	'NAV_BAR_CRUMBS_AFTER'				=> 'Navigaatiopalkissa polun jälkeen',
	'FOOTER_TIMEZONE_BEFORE'			=> 'Alapalkissa ennen aikavyöhykettä',
	'FOOTER_TIMEZONE_AFTER'				=> 'Alapalkissa aikavyöhykkeen jälkeen',
	'FOOTER_TEAMS_BEFORE'				=> 'Alapalkissa ennen Ryhmät-linkkiä',
	'FOOTER_TEAMS_AFTER'				=> 'Alapalkissa Ryhmät-linkin jälkeen',
	'QUICK_LINK_MENU_BEFORE'			=> 'Pikalinkkivalikon alussa',
	'QUICK_LINK_MENU_AFTER'				=> 'Pikalinkkivalikon lopussa',
));
