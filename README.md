See below for English description.

# Suomennos Pages-laajennukselle

Suomenkielinen kielipaketti phpBB:n Pages-laajennukselle.

## Asennus

1. Asenna Pages-laajennus ensin.

2. Lataa laajennuksen versiota vastaava kielipaketti alla olevasta luettelosta:

	- [2.0.1 – 2.0.2](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-2.0.1.zip)
	- [2.0.0](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-2.0.0.zip)
	- [1.0.5](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-1.0.5.zip)
	- [1.0.4](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-1_0_4.7z)
	- [1.0.3](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-1_0_3.7z)

3. Pura paketin sisältö phpBB:n `ext/phpbb/pages`-hakemistoon.

# Finnish Translation for Pages

A Finnish language pack for the Pages extension for phpBB.

## Installation

1. Install the Pages extension first.

2. Download the language pack matching the version of the extension from the
list below:

	- [2.0.1 – 2.0.2](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-2.0.1.zip)
	- [2.0.0](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-2.0.0.zip)
	- [1.0.5](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-1.0.5.zip)
	- [1.0.4](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-1_0_4.7z)
	- [1.0.3](https://bitbucket.org/spekkala/finnish-translation-for-pages-phpbb/downloads/pages-fi-1_0_3.7z)

3. Extract the contents of the archive into the `ext/phpbb/pages`
directory under your phpBB root directory.
