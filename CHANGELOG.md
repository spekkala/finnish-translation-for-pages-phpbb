# Change Log

## 2.0.1 (2018-01-06)

- Update translations to be compatible with Pages 2.0.1.

## 2.0.0 (2017-02-26)

- Update translations to be compatible with Pages 2.0.0.

## 1.0.5 (2017-02-26)

- Include extension version 1.0.5 in `README.md`.

## 1.0.4 (2016-03-14)

- Include extension version 1.0.4 in `README.md`.

## 1.0.3 (2015-10-06)

- Initial release.
